# import vega
import torch.nn as nn
import torch
# from zeus.common.config import Config
import sys
class Simplenn(nn.Module):
   
    def __init__(self, argv):
        """Initialize."""
        super(Simplenn, self).__init__()
        desc = argv
        self.input_features=int(desc["input_features"]) 
        self.num_class = int(desc["num_class"])
        self.fc = nn.Linear(self.input_features, self.num_class,dtype=torch.float)
    def forward(self, x):
        """Forward."""
        x = self.fc(x.view(x.size(0), -1))
        return x
if __name__ == "__main__":
    import yaml
    argv={}
    with open("./demo.yaml",encoding='utf-8') as f:
        argv=yaml.safe_load(f)
    ocp=Simplenn(argv)
    x=torch.randint(1,10,(1,ocp.input_features),dtype=torch.float)
    print(x)
    print(ocp(x))
